---
layout: handbook-page-toc
title: MLOps Incubation Engineering
---

## MLOps Single-Engineer Group

DRI: [@eduardobonet](https://gitlab.com/eduardobonet)

MLOps is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation/). This group works on early feature exploration and validation related to the [MLOps group](/direction/modelops/mlops) within the [ModelOps stage](/direction/modelops/).

## Vision & Mission

**Mission**: Make GitLab a tool Data Scientists and Machine Learning Engineers love to use.

**Vision**: Identify opportunities in our portfolio to explore ways where GitLab can provide a better user experience for Data Science and Machine Learning across the entire Machine Learning life cycle (model creation, testing, deployment, monitoring, and iteration).

## Updates

July 28th 2022: Jobs to Be Done, Usage, and started work on MLFlow! [Issue](https://gitlab.com/gitlab-org/incubation-engineering/mlops/meta/-/issues/59)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/NUEIUJENClM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

[Subscribe to the issue for updates](https://gitlab.com/gitlab-org/incubation-engineering/mlops/meta/-/issues/16)

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Backlog

https://gitlab.com/groups/gitlab-org/incubation-engineering/mlops/-/epics/8
